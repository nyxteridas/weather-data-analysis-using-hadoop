/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.hadoopEMY;

import java.util.*;
import java.io.IOException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;
import java.util.Iterator;

/**
 *
 * @author Georgios Kavvadias & Spiros Lepeniotis
 */
public class ProcessEMYAverageYearSeasonTown {

	// Mapper class for Average temperature per year, per season per town
	public static class MyMapperAvg extends MapReduceBase implements
			Mapper<LongWritable, /* Input key Type */ Text, /*
															 * Input value Type
															 */ Text, /*
																		 * Output
																		 * key
																		 * Type
																		 */ Text> /*
																				 * Output
																				 * value
																				 * Type
																				 */ {

		private Text word = new Text();
		private Text values = new Text();

		// Map function
		public void map(LongWritable key, Text value, OutputCollector<Text, Text> output, Reporter reporter)
				throws IOException {

			// read lines from file
			String line = value.toString();
			// split each line into its fields (delimiter = ";")
			String[] s = line.split("\\;");

			// index for current field
			int counter = 0;
			// key
			String keyOut = null;
			// value
			String valueStr = null;
			// flag indicating if a line is valid and is allowed to be emitted
			boolean skip = false;

			// scan the fields of the line read
			while (counter < s.length) {
				String str = s[counter];

				switch (counter) {

				case 0: // StationID
					// convert stationID to town name, and use it as key
					keyOut = getTown(str);
					break;
				case 1: // year
					/*
					 * if year is present, include it in the key, separated with
					 * "_" from the previous field
					 */
					if (str != null && !(str.trim()).equals("")) {
						keyOut = keyOut.concat("_") + str;
					}
					break;
				case 2: // month
					/*
					 * if month is present, convert it to corresponding season
					 * and include it in the key, separated with "_" from the
					 * previous fields
					 */
					if (str != null && !(str.trim()).equals("")) {
						String season = getSeason(Integer.parseInt(str));
						if (!season.equals("skip")) {
							keyOut = keyOut.concat("_") + season;
						} else {
							// if the season is not valid, the line should be
							// skipped
							skip = true;
						}
					}
					break;
				case 8: // max temperature
					/*
					 * if temperature is present, replace "," with "." in order
					 * to be compatible with Double data type otherwise line
					 * should be skipped after this include it in the value
					 */
					if (str != null && !(str.trim()).equals("")) {
						valueStr = str.replace(",", ".");
					} else {
						skip = true;
					}
					break;
				default:
					break;
				}
				counter++;
			}

			// in case all our data are valid, emit key-value pair
			if (!skip) {
				word.set(keyOut);
				try {
					values.set(valueStr);
				} catch (NullPointerException ex) {
					System.out.println(valueStr + " " + keyOut);
					ex.printStackTrace();
				}
				output.collect(word, values);
			}
		}
	}

	// Reducer class for Average temperature per year, per season per town
	public static class MyReducerAvg extends MapReduceBase implements Reducer<Text, Text, Text, Text> {
		// Reduce function

		private Text valueOutText = new Text();

		public void reduce(Text key, Iterator<Text> values, OutputCollector<Text, Text> output, Reporter reporter)
				throws IOException {

			// initialize sum variable used for calculating Average
			Double sum_temp = 0.0;
			// counter used to calculate Average
			int count = 0;

			// scan input and summarize all valid temperature values
			while (values.hasNext()) {

				String str = values.next().toString();
				if (str != null && !(str.trim()).equals("")) {
					sum_temp += Double.parseDouble(str);
					count++;
				}

			}

			// calculate average and emit it along with the key
			double avg_tmp = sum_temp / count;
			String valueOut = Double.toString(avg_tmp);
			valueOutText.set(valueOut);
			output.collect(key, valueOutText);
		}
	}

	// Function for getting season
	private static String getSeason(Integer month) {
		// return season name, according to given month number
		switch (month) {
		case 6:
		case 7:
		case 8:
			return "Summer";
		case 9:
		case 10:
		case 11:
			return "Autumn";
		case 12:
		case 1:
		case 2:
			return "Winter";
		case 3:
		case 4:
		case 5:
			return "Spring";
		default:
			return "skip";
		}
	}

	// Function for getting station's town name
	private static String getTown(String stationID) {

		// return town name, according to given stationID
		switch (stationID) {

		case "16627":
			return "Alexandroupoli";
		case "16746":
			return "Chania";
		case "16716":
			return "Elliniko";
		case "16754":
			return "Hrakleio";
		case "16642":
			return "Ioannina";
		case "16726":
			return "Kalamata";
		case "16641":
			return "Kerkyra";
		case "16743":
			return "Kythira";
		case "16648":
			return "Larisa";
		case "16650":
			return "Limnos";
		case "16734":
			return "Methoni";
		case "16738":
			return "Milos";
		case "16667":
			return "Mytilini";
		case "16732":
			return "Naxos";
		case "16689":
			return "Patra";
		case "16749":
			return "Rodos";
		case "16723":
			return "Samos";
		case "16684":
			return "Skyros";
		case "16622":
			return "Thessaloniki";
		case "16710":
			return "Tripoli";
		case "16719":
			return "Zakynthos";
		default:
			return "";
		}
	}

	// Main function
	public static void main(String args[]) throws Exception {
		JobConf conf = new JobConf(ProcessEMYAverageYearSeasonTown.class);

		// set job name
		conf.setJobName("AverageTemp_perYear_perSeason_perTown");

		// set output key and output value types
		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(Text.class);

		// set mapper class and reducer class
		conf.setMapperClass(MyMapperAvg.class);
		conf.setReducerClass(MyReducerAvg.class);

		// set input and output format
		conf.setInputFormat(TextInputFormat.class);
		conf.setOutputFormat(TextOutputFormat.class);

		// read from args[] the input and output paths
		FileInputFormat.setInputPaths(conf, new Path(args[0]));
		FileOutputFormat.setOutputPath(conf, new Path(args[1]));

		// run the job
		JobClient.runJob(conf);
	}
}
