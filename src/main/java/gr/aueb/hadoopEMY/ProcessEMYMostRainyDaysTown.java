/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.hadoopEMY;

import java.util.*;
import java.io.IOException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;
import java.util.Iterator;

/**
 *
 * @author Georgios Kavvadias & Spiros Lepeniotis
 */
public class ProcessEMYMostRainyDaysTown {

	// Mapper class most rainy days per town
	public static class MyMapperMax extends MapReduceBase implements
			Mapper<LongWritable, /* Input key Type */ Text, /*
															 * Input value Type
															 */ Text, /*
																		 * Output
																		 * key
																		 * Type
																		 */ Text> /*
																				 * Output
																				 * value
																				 * Type
																				 */ {

		private Text word = new Text();
		private Text values = new Text();

		// Map function
		public void map(LongWritable key, Text value, OutputCollector<Text, Text> output, Reporter reporter)
				throws IOException {

			// read lines from file
			String line = value.toString();
			// split each line into its fields (delimiter = ";")
			String[] s = line.split("\\;");

			// index for current field
			int counter = 0;
			// key
			String keyOut = null;
			// value
			String valueStr = null;
			// flag indicating if a line is valid and is allowed to be emitted
			boolean skip = false;

			// scan the fields of the line read
			while (counter < s.length) {
				String str = s[counter];

				switch (counter) {

				case 0: // StationID
					// convert stationID to town name, and use it as key
					keyOut = getTown(str);
					break;
				case 1: // year
					// if year is present, include it in the value
					if (str != null && !(str.trim()).equals("")) {
						valueStr = str;
					}
					break;
				case 2: // month
					/*
					 * if month is present, convert it in two digits number and
					 * include it in the value, separated with "/" from the
					 * previous field
					 */
					if (str != null && !(str.trim()).equals("")) {
						if (Integer.parseInt(str) < 10)
							valueStr = valueStr.concat("/").concat("0" + str);
						else
							valueStr = valueStr.concat("/").concat(str);
					}
					break;
				case 3: // day
					/*
					 * if day is present, convert it in two digits number and
					 * include it in the value, separated with "/" from the
					 * previous fields
					 */
					if (str != null && !(str.trim()).equals("")) {
						if (Integer.parseInt(str) < 10)
							valueStr = valueStr.concat("/").concat("0" + str);
						else
							valueStr = valueStr.concat("/").concat(str);
					}
					break;
				case 4: // hour
					/*
					 * if hour is present, convert it in full time format
					 * (HH:mm:ss) and include it in the value, separated with
					 * " " from the previous fields
					 */
					if (str != null && !(str.trim()).equals("")) {
						if (Integer.parseInt(str) < 10)
							valueStr = valueStr.concat(" ").concat("0" + str + ":00:00");
						else
							valueStr = valueStr.concat(" ").concat(str + ":00:00");
					}
					break;
				case 15: // precip height
					/*
					 * if precipitation height is present, replace "," with "."
					 * in order to be compatible with Double data type otherwise
					 * line should be skipped after this include it in the value
					 * separated with " " from the previous fields
					 */
					if (str != null && !(str.trim()).equals("")) {
						valueStr = valueStr.concat("_").concat(str.replace(",", "."));
					} else {
						skip = true;
					}
					break;
				default:
					break;
				}
				counter++;
			}

			// in case all our data are valid, emit key-value pair
			if (!skip) {
				word.set(keyOut);
				values.set(valueStr);
				output.collect(word, values);
			}
		}
	}

	// Reducer class most rainy days per town
	public static class MyReducerMax extends MapReduceBase implements Reducer<Text, Text, Text, Text> {
		// Reduce function

		private Text valueOutText = new Text();

		public void reduce(Text key, Iterator<Text> values, OutputCollector<Text, Text> output, Reporter reporter)
				throws IOException {

			/*
			 * initialize max precipitation variable to a very small value
			 * (almost invalid for this type of measurement)
			 */
			double maxPrecip = -100.0;
			// initialize the corresponding dateTime to empty string
			String dateTime = "";

			// scan input and compare values
			while (values.hasNext()) {

				String str = values.next().toString();
				// split input values to its fields using "_" as delimiter
				StringTokenizer itr = new StringTokenizer(str, "_");

				// initialize the index of fields
				int countVector = 0;
				/*
				 * temp will be used to store the dateTime before we assure that
				 * it is connected with the correct precipitation
				 */
				String temp = "";
				while (itr.hasMoreTokens()) {
					String nextToken = itr.nextToken();
					// hold dateTime to the temp variable
					if (countVector == 0) {
						temp = nextToken;
					}
					/*
					 * check if precipitation height is bigger than the one we
					 * already have in this case, change also the dateTime we
					 * already have, with the one connected with the new max
					 */
					else if (countVector == 1) {
						if (maxPrecip < Double.parseDouble(nextToken)) {
							dateTime = temp;
							maxPrecip = Double.parseDouble(nextToken);
						}
					}
					countVector++;
				}
			}

			// emit new key-value pairs
			String valueOut = dateTime.concat("\t") + maxPrecip;
			valueOutText.set(valueOut);
			output.collect(key, valueOutText);
		}
	}

	// Function for getting station's town name
	private static String getTown(String stationID) {

		// return town name, according to given stationID
		switch (stationID) {

		case "16627":
			return "Alexandroupoli";
		case "16746":
			return "Chania";
		case "16716":
			return "Elliniko";
		case "16754":
			return "Hrakleio";
		case "16642":
			return "Ioannina";
		case "16726":
			return "Kalamata";
		case "16641":
			return "Kerkyra";
		case "16743":
			return "Kythira";
		case "16648":
			return "Larisa";
		case "16650":
			return "Limnos";
		case "16734":
			return "Methoni";
		case "16738":
			return "Milos";
		case "16667":
			return "Mytilini";
		case "16732":
			return "Naxos";
		case "16689":
			return "Patra";
		case "16749":
			return "Rodos";
		case "16723":
			return "Samos";
		case "16684":
			return "Skyros";
		case "16622":
			return "Thessaloniki";
		case "16710":
			return "Tripoli";
		case "16719":
			return "Zakynthos";
		default:
			return "";
		}
	}

	// Main function
	public static void main(String args[]) throws Exception {
		JobConf conf = new JobConf(ProcessEMYMostRainyDaysTown.class);

		// set job name
		conf.setJobName("mostRainyDays_perTown_AllYears");

		// set output key and output value types
		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(Text.class);

		// set mapper class and reducer class
		conf.setMapperClass(MyMapperMax.class);
		conf.setReducerClass(MyReducerMax.class);

		// set input and output format
		conf.setInputFormat(TextInputFormat.class);
		conf.setOutputFormat(TextOutputFormat.class);

		// read from args[] the input and output paths
		FileInputFormat.setInputPaths(conf, new Path(args[0]));
		FileOutputFormat.setOutputPath(conf, new Path(args[1]));

		// run the job
		JobClient.runJob(conf);
	}
}
