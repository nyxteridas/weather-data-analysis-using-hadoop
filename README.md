## Hadoop-weather-statistics v1.0

Analyzing data from Hellenic National Meteorological Service.
This work was done along with my colleague Spiros Lepeniotis, within the context of Master Program "Information Systems" @ AUEB

---

## Introduction

In this paper we analyze the primary data of the Hellenic National Meteorological Service which were collected between 1975 and 2004, from 21 meteorological stations of the country.
We created a 6-node cluster in which the Hadoop system run on a "Shared data storage" architecture. The original data were distributed to the nodes using appropriate methods. With the use of "MapReduce" data were analyzed and weather statistics were created regarding the Greek territory per town, per county, per time period etc.

In the base folder you can find also an example file from the primary data used.

---

## Query explanation and matching with classes

The application in general creates one single jar with all the needed classes.
While running it, you define which type of measurement you wish to take and were the input/output folder is.
Below we are describing in high level the functionality. You can find more details in the comments of the source code.

## 1. Minimum-Maximum temperature from 1975 to 2004 in the country, when and where it appeared

The execution commands are shown below:

**yarn jar HadoopEMY-1.0-SNAPSHOT.jar gr.aueb.hadoopEMY.ProcessEMYMinMaxCountryAll min /hadoop/inputEMY /hadoop/outputEMY**

**yarn jar HadoopEMY-1.0-SNAPSHOT.jar gr.aueb.hadoopEMY.ProcessEMYMinMaxCountryAll max /hadoop/inputEMY /hadoop/outputEMY**

Where:

1. **"yarn"** is the central execution command of the job in the cluster,
2. **"jar"** is the type of executable file that contains our code, followed by the file name and execution class
3. Finally, the execution parameters follow: **min/max**, the path to the folder containing the **inputs**, and the path to the folder where the **output** will be stored. If the output folder already exists, the program will not proceed.

**Implementation:**

1. The grouping is based on the country, and for the whole range of the date range.
2. The key is "Greece" and based on this the map function works.

**Description of the Key - Value pair:**

1. The map reads the files it has in the inputEMY folder per line.
2. Separates each line in its fields based on the delimiter ";".

**Fields used:**

1. **STATIONID**: Indicates to which meteorological station the temperature (lower or higher) occurred. Stations are refered by a code in the input fies, for this reason a function has been created that accepts the station code as input and returns the name of the city to which it belongs.
2. **OBSERVATIONYEAR, OBSERVATIONMONTH, OBSERVATIONDAY, OBSERVATIONHOUR**: based on which we form the timestamp in which the measurement was taken.
3. **MAXTEMP, MINTEMP**: depending on whether our class is executed to find the minimum or maximum temperature. According to our data, stations receive measurements every 3 hours, but the temperature is only taken every 12 hours. Therefore any line that does not include temperature is bypassed.

The value extracted by the map consists of the city name to which the station belongs, the timestamp, and the temperature. All these separated by "_".
The reduce function accepts the key-value pairs that the map has extracted and for each key it searches for the lowest or higher temperature of all the pairs associated with that key.


## 2. Minimum-Maximum temperature from 1975 to 2004 by city and when it appeared

The execution commands are shown below:

**yarn jar HadoopEMY-1.0-SNAPSHOT.jar gr.aueb.hadoopEMY.ProcessEMYMinMaxTownAll min /hadoop/inputEMY /hadoop/outputEMY**

**yarn jar HadoopEMY-1.0-SNAPSHOT.jar gr.aueb.hadoopEMY.ProcessEMYMinMaxTownAll max /hadoop/inputEMY /hadoop/outputEMY**

Where:

1. **"yarn"** is the central execution command of the job in the cluster,
2. **"jar"** is the type of executable file that contains our code, followed by the file name and execution class
3. Finally, the execution parameters follow: **min/max**, the path to the folder containing the **inputs**, and the path to the folder where the **output** will be stored. If the output folder already exists, the program will not proceed.

**Implementation:**

1. The grouping is based on the city and the whole range of the date range.
2. The key is the name of the city and based on it the map function will work.

**Description of the Key - Value pair:**

1. The map reads the files it has in the inputEMY folder per line.
2. Separates each line in its fields based on the delimiter ";".

**Fields:**

1. **STATIONID**: Indicates to which meteorological station the temperature (lower or higher) occurred. Stations are refered by a code in the input fies, for this reason a function has been created that accepts the station code as input and returns the name of the city to which it belongs.
2. **OBSERVATIONYEAR, OBSERVATIONMONTH, OBSERVATIONDAY, OBSERVATIONHOUR**: based on which we form the timestamp in which the measurement was taken.
3. **MAXTEMP, MINTEMP**: depending on whether our class is executed to find the minimum or maximum temperature. According to our data, stations receive measurements every 3 hours, but the temperature is only taken every 12 hours. Therefore any line that does not include temperature is bypassed.

The key extracted by the map consists of the city name to which the station belongs, and the value from the timestamp, and the temperature. Value fields are separated by "_".
The reduce function accepts the key-value pairs that the map has extracted and for each key it searches for the lowest or higher temperature of all the pairs associated with that key.


## 3. The minimum-maximum temperature per decade in the country, when and where it appeared

The execution commands are shown below:

**yarn jar HadoopEMY-1.0-SNAPSHOT.jar gr.aueb.hadoopEMY.ProcessEMYMinMaxCountryDecade min /hadoop/inputEMY /hadoop/outputEMY**

**yarn jar HadoopEMY-1.0-SNAPSHOT.jar gr.aueb.hadoopEMY.ProcessEMYMinMaxCountryDecade max /hadoop/inputEMY /hadoop/outputEMY**

Where:

1. **"yarn"** is the central execution command of the job in the cluster,
2. **"jar"** is the type of executable file that contains our code, followed by the file name and execution class
3. Finally, the execution parameters follow: **min/max**, the path to the folder containing the **inputs**, and the path to the folder where the **output** will be stored. If the output folder already exists, the program will not proceed.

**Implementation:**

1. The grouping is based on country and decade.
2. The key is the name of the country "Greece" and the decade we are talking about, separated by "_" e.g. "Greece_1970-1979".

**Description of the Key - Value pair:**

1. The map reads the files it has in the inputEMY folder per line.
2. Separates each line in its fields based on the delimiter ";".

**Fields:**

1. **STATIONID**: Indicates to which meteorological station the temperature (lower or higher) occurred. Stations are refered by a code in the input fies, for this reason a function has been created that accepts the station code as input and returns the name of the city to which it belongs.
2. **OBSERVATIONYEAR, OBSERVATIONMONTH, OBSERVATIONDAY, OBSERVATIONHOUR**: based on which we form the timestamp in which the measurement was taken.
3. **MAXTEMP, MINTEMP**: depending on whether our class is executed to find the minimum or maximum temperature. According to our data, stations receive measurements every 3 hours, but the temperature is only taken every 12 hours. Therefore any line does not include temperature bypassed.

The value extracted by the map consists of the city name to which the station belongs, the timestamp, and the temperature. Value fields are separated by "_".
The reduce function accepts the key-value pairs that the map has extracted and for each key it searches for the lowest or higher temperature of all the pairs associated with that key.


## 4. The minimum-maximum temperature per decade, per city and when it appeared

The execution commands are shown below:

**yarn jar HadoopEMY-1.0-SNAPSHOT.jar gr.aueb.hadoopEMY.ProcessEMYMinMaxTownDecade min /hadoop/inputEMY /hadoop/outputEMY**

**yarn jar HadoopEMY-1.0-SNAPSHOT.jar gr.aueb.hadoopEMY.ProcessEMYMinMaxTownDecade max /hadoop/inputEMY /hadoop/outputEMY**

Where:

1. **"yarn"** is the central execution command of the job in the cluster,
2. **"jar"** is the type of executable file that contains our code, followed by the file name and execution class
3. Finally, the execution parameters follow: **min/max**, the path to the folder containing the **inputs**, and the path to the folder where the **output** will be stored. If the output folder already exists, the program will not proceed.

**Implementation:**

1. The grouping is based on the city and the decade.
2. The key is the name of the city and the decade we are talking about, separated by "_" e.g "Tripoli_1970-1979".

**Description of the Key - Value pair:**

1. The map reads the files it has in the inputEMY folder per line.
2. Separates each line in its fields based on the delimiter ";".

**Fields:**

1. **STATIONID**: Indicates to which meteorological station the temperature (lower or higher) occurred. Stations are refered by a code in the input fies, for this reason a function has been created that accepts the station code as input and returns the name of the city to which it belongs.
2. **OBSERVATIONYEAR, OBSERVATIONMONTH, OBSERVATIONDAY, OBSERVATIONHOUR**: based on which we form the timestamp in which the measurement was taken.
3. **MAXTEMP, MINTEMP**: depending on whether our class is executed to find the minimum or maximum temperature. According to our data, stations receive measurements every 3 hours, but the temperature is only taken every 12 hours. Therefore any line does not include temperature bypassed.

The value extracted by the map consists of the timestamp, and the temperature. Value fields are separated by "_".
The reduce function accepts the key-value pairs that the map has extracted and for each key it searches for the lowest or higher temperature of all the pairs associated with that key.


## 5. The coolest winter in Greece / the hottest summer in Greece

The execution commands are shown below:

**yarn jar HadoopEMY-1.0-SNAPSHOT.jar gr.aueb.hadoopEMY.ProcessEMYColderWinterHotterSummer winter /hadoop/inputEMY /hadoop/outputEMY**

**yarn jar HadoopEMY-1.0-SNAPSHOT.jar gr.aueb.hadoopEMY.ProcessEMYColderWinterHotterSummer summer /hadoop/inputEMY /hadoop/outputEMY**

Where:

1. **"yarn"** is the central execution command of the job in the cluster,
2. **"jar"** is the type of executable file that contains our code, followed by the file name and execution class
3. Finally, the execution parameters follow: **winter/summer**, the path to the folder containing the **inputs**, and the path to the folder where the **output** will be stored. If the output folder already exists, the program will not proceed.

**Implementation:**

1. The grouping is based on country and season.
2. The key is the name of the country and the season we are talking about, separated by "_" e.g "Greece_Winter".

**Description of the Key - Value pair:**

1. The map reads the files it has in the inputEMY folder per line.
2. Separates each line in its fields based on the delimiter ";".

**Fields:**

1. **STATIONID**: Indicates to which meteorological station the temperature (lower or higher) occurred. Stations are refered by a code in the input fies, for this reason a function has been created that accepts the station code as input and returns the name of the city to which it belongs.
2. **OBSERVATIONYEAR, OBSERVATIONMONTH, OBSERVATIONDAY, OBSERVATIONHOUR**: based on which we form the timestamp in which the measurement was taken.
3. **MAXTEMP, MINTEMP**: depending on whether our class is executed to find the coolest winter or hottest summer. According to our data, stations receive measurements every 3 hours, but the temperature is only taken every 12 hours. Therefore any line does not include temperature bypassed.

The value extracted by the map consists of the timestamp, the temperature and the city. Value fields are separated by "_".
The reduce function accepts the key-value pairs the map has extracted and for each key it searches for the lowest (for winter) or higher (for summer) temperature of all pairs associated with that key.


## 6. Average temperature per year, per season, per meteorological station

The execution command are shown below:

**yarn jar HadoopEMY-1.0-SNAPSHOT.jar gr.aueb.hadoopEMY.ProcessEMYAverageYearSeasonTown /hadoop/inputEMY /hadoop/outputEMY**

Where:

1. **"yarn"** is the central execution command of the job in the cluster,
2. **"jar"** is the type of executable file that contains our code, followed by the file name and execution class
3. Finally, the execution parameters follow: the path to the folder containing the **inputs**, and the path to the folder where the **output** will be stored. If the output folder already exists, the program will not proceed.

**Implementation:**

1. The grouping is based on the city, the year and the season.
2. The key is the name of the city, the year and the season for which we are talking, separated by "_" such as "Zakynthos_2000_Spring".

**Description of the Key - Value pair:**

1. The map reads the files it has in the inputEMY folder per line.
2. Separates each line in its fields based on the delimiter ";".

**Fields:**

1. **STATIONID**: Indicates to which meteorological station the temperature (lower or higher) occurred. Stations are refered by a code in the input fies, for this reason a function has been created that accepts the station code as input and returns the name of the city to which it belongs.
2. **OBSERVATIONYEAR**: this is the second part of the key
3. **OBSERVATIONMONTH**: this is the third part of the key and from which the season derrives. In order to extract the season from the month, a seperate function has been created which accepts the number of the month and returns the name of the season.
4. **MAXTEMP, MINTEMP**: depending on whether our class is executed to find the coolest winter or hottest summer. According to our data, stations receive measurements every 3 hours, but the temperature is only taken every 12 hours. Therefore any line does not include temperature bypassed.

Our key consists of the city, the year and the season, and the value of the temperature.
The reduce function accepts the key-value pairs extracted by the map and for each key, it calculates the average of the temperatures associated with that key.


## 7. The most rainy days per city

The execution commands are shown below:

**yarn jar HadoopEMY-1.0-SNAPSHOT.jar gr.aueb.hadoopEMY.ProcessEMYMostRainyDaysTown /hadoop/inputEMY /hadoop/outputEMY**

Where:

1. **"yarn"** is the central execution command of the job in the cluster,
2. **"jar"** is the type of executable file that contains our code, followed by the file name and execution class
3. Finally, the execution parameters follow: the path to the folder containing the **inputs**, and the path to the folder where the **output** will be stored. If the output folder already exists, the program will not proceed.

**Implementation:**

1. The grouping is based on the city.
2. The key is the name of the city we are talking about.

**Description of the Key - Value pair:**

1. The map reads the files it has in the inputEMY folder per line.
2. Separates each line in its fields based on the delimiter ";".

**Fields:**

1. **STATIONID**: Indicates to which meteorological station the temperature (lower or higher) occurred. Stations are refered by a code in the input fies, for this reason a function has been created that accepts the station code as input and returns the name of the city to which it belongs.
2. **OBSERVATIONYEAR, OBSERVATIONMONTH, OBSERVATIONDAY, OBSERVATIONHOUR**: based on which we form the timestamp in which the measurement was taken.
3. **PRECIPHEIGHT**: by which we take the amount of rainfall. Because obviously there is no rainfall at each measurement, any line that does not include a value is bypassed (this field is counted in mm, 1mm = 1lt of water per square meter).

Our key consists of the timestamp and amount of rainfall.
The reduce function accepts the key-value pairs that the map has extracted and for each key it calculates the largest amount of rain associated with that key.

